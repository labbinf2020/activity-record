## Date: 06/11/2020

### Previous Week

* [Group 1](1): Getting sequences into a matrix
* [Group 2](2): Defining relations between tables and attributes
* [Group 3](3): Working with raxML and Mrbayes
* [Group 4.1](4.1): Kept working on the script for the random Missing Data
* [Group 4.2](4.2): Almost done with the 1st task
* [Group 4.3](4.3): Worked on the multiplex group
* [Group 5](5): Nothing to report (missed class)
* [Group 6](6): Read papers
* [Group 7](7): Docker on git ready
* [Group 9](9): Research for the website

### Today

* [Group 1](1): Kept doing the same thing
* [Group 2](2): New versions of the db schema and new versions of the db's script 
* [Group 3](3): Kept doing the same thing
* [Group 4.1](4.1): Did the same thing
* [Group 4.2](4.2): Finishing the said task
* [Group 4.3](4.3): Did the same thing
* [Group 5](5): Nothing to report (missed class)
* [Group 6](6):Kept analysing papers and started to write our paper
* [Group 7](7): Working on the project's workflow with snakemake
* [Group 9](9): Working on the website 

---

## Date 13/11/2020

### Previous week 

* [Group 1](1):
* [Group 2](2): Changing the Bd schema again (changing the order of the relations and some tables/attributes)
* [Group 3](3):
* [Group 4.1](4.1): Script for random (commited fo git)
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Script to convert from fasta to lists (commited to git)
* [Group 5](5): Finding out the right tools for the tree comparison
* [Group 6](6): Kept investigating older papers and writing our own 
* [Group 7](7): Has everything done and is waiting for the 1st and 3rd groups 
* [Group 9](9): Bug correction and working on the website configurations


## Today

* [Group 1](1):
* [Group 2](2): Finishing the BD script and uploading it on docker
* [Group 3](3): Working on the shell script
* [Group 4.1](4.1): Working on the random script 
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Helping on other scripts
* [Group 5](5): Finding out the right tools for the tree comparison
* [Group 6](6): Kept investigating older papers and writing our own
* [Group 7](7): Looking to start tests and finishing the overflow script
* [Group 9](9): Bug correction and working on the website configurations



---


## Date 20/11/2020

### Previous week 

* [Group 1](1): Got the data together on a json file 
* [Group 2](2): Research for the "inputs to db" python script 
* [Group 3](3): Researched alternatives for figtree and how to get more detailed raxML outputs
* [Group 4.1](4.1): Worked on the input of missing data script (commited)
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Nothing to report (missed class)
* [Group 5](5): Nothing to report (missed class)
* [Group 6](6): Worked on the same thing
* [Group 7](7): Got some rules of the workflow to be fully functional
* [Group 9](9): Research for the website


## Today

* [Group 1](1): Created the final versions of the json files 
* [Group 2](2): Trying to put the BD online / Starting the "DB inputs" python script
* [Group 3](3): Worked on those more detailed raxML outputs
* [Group 4.1](4.1): Developed the "missing data input script"
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Nothing to report (missed class)
* [Group 5](5): Nothing to report (missed class)
* [Group 6](6): Worked with group 1 on datasets
* [Group 7](7): Developed snakemake rules
* [Group 9](9): Started to build the app


---


## Date: 27/11/2020

### Previous Week

* [Group 1](1): Got new papers/started to check the number of p.b's on each dataset
* [Group 2](2): Continued the insert to db script (commited) / BD is already on docker 
* [Group 3](3): Alternatives to figtree
* [Group 4.1](4.1): Kept working on the script for the random Missing Data (commited)
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Nothing to report (missed class)
* [Group 5](5): Started to work with Consel
* [Group 6](6): Miguel helped group 1 to get some functions working (commited)
* [Group 7](7): Kept developing the snakemake script (commited)
* [Group 9](9): Learning django



### Today

* [Group 1](1): Got new json files ready
* [Group 2](2): Tryed to finish the insert to db script (commited)
* [Group 3](3): Kept doing the same thing
* [Group 4.1](4.1): Kept working on the Missing Data script (commited)
* [Group 4.2](4.2): Nothing to report (missed class)
* [Group 4.3](4.3): Nothing to report (missed class)
* [Group 5](5): Kept working with Consel
* [Group 6](6): Miguel helped group 1
* [Group 7](7): Developing snakemake script (commited)
* [Group 9](9): Starting to design the website 

---

## Date: 04/12/2020


### Today

* [Group 1](1): Got more files ready for analysis and worked on the script to generate fasta files 
* [Group 2](2): Finalizing the input to db script and started to work on how will the db be online
* [Group 3](3): Worked with the ETE tools
* [Group 4.1](4.1): Kept working on the script for the random Missing Data (com$
* [Group 4.2](4.2): Kept working on the missing data script 
* [Group 4.3](4.3): Worked with group 5
* [Group 5](5): Working on IQTree
* [Group 6](6): Miguel helped group 1 on the said script (commited)
* [Group 7](7): Kept developing the snakemake script (commited)
* [Group 9](9): Learning django





## Date: 11/12/2020

### Today

* [Group 1](1): Got more files ready for analysis and worked on the script to g$
* [Group 2](2): Got the first part of the input to db from json script done
* [Group 3](3): Worked with the ETE tools
* [Group 4.1](4.1): Kept working on the script for the random Missing Data (com$
* [Group 4.2](4.2): Kept working on the missing data script
* [Group 4.3](4.3): Worked with group 5
* [Group 5](5): Working on IQTree
* [Group 6](6): Miguel helped group 1 on the said script (commited) + Reading papers and writing our paper
* [Group 7](7): Kept developing the snakemake script (commited)
* [Group 9](9): Learning django








---

##Meeting on Discord

### Date: 29/12/2020

* [Group 1](1): Is missing the commits for the file and the script 
* [Group 2](2): Bd schema done, input script for fasta done, input script for json 90%, docker 
* [Group 3](3): Delayed script for ETE (needs help)
* [Group 4.1](4.1): Random script working but needs refining
* [Group 4.2](4.2): Delayed script for ETE (needs help) 
* [Group 4.3](4.3): Script doesn't work (needs help)
* [Group 5](5): Working but needed to check for a command to insert in the automatization process
* [Group 6](6): On schedule 
* [Group 7](7): Everything done that can be done so far and will work with group 5
* [Group 9](9): Will continue to develop the website with the bd schema

*NOTE: Groups 4 will help each other and group 3 will be helped by Daniel and Bruno






## Legenda dos grupos

[1](https://gitlab.com/labbinf2020/datasets)
[2](https://gitlab.com/labbinf2020/database)
[3](https://gitlab.com/labbinf2020/scripts)
[4.1](https://gitlab.com/labbinf2020/scripts)
[4.2](https://gitlab.com/labbinf2020/scripts)
[4.3](https://gitlab.com/labbinf2020/scripts) 
[6](Miguel/Catarina Canastra/ Catarina Rodrigues -- Paper)
[7](https://gitlab.com/labbinf2020/workflow)
[9](https://gitlab.com/labbinf2020/website)
[8] Project Managers- Marcelo e Miguel 
